$(document).ready(function(){
    var i = $('input[type=file]').size() +1;

    $('.add').click(function() {
//        $('<div class="pole"><input type="file" name="fl[]" /><a class="btn btn-danger delete">X</a><div class="imagePreview"></div></div>').fadeIn('slow').appendTo('div.inputs');
        $('<div class="pole"><img src="#" class="imagePreview" style="display:none;"/><input type="file" name="fl[]" /><a class="btn btn-danger delete">X</a></div>').fadeIn('slow').appendTo('div.inputs');
    });

    $('body').on('click', 'a.delete', function() {
        $(this).parent().fadeOut('slow',function(){
            $(this).remove();
        });
        //$(this).parent().remove();
        return false;
    });

    $('#reset').click(function() {
        while(i > 2) {
            $('.field:last').remove();
            i--;
        }
    });

    $('.submit').click(function(){
        var answers = [];

        $.each($('.field'), function() {
            answers.push($(this).val());
        });

        if(answers.length == 0) {
            answers = "none";
        }
        alert(answers);
        return false;
    });

    $("body").on("change", "input[type=file]", function()
    {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            var input = this;
            reader.onloadend = function(){
                //$(input).parent('div').find(".imagePreview").css("background-image", "url("+this.result+")");
                $(input).parent('div').find("img.imagePreview").show();
                $(input).parent('div').find("img.imagePreview").attr("src", this.result);
            }
        }
    });
});