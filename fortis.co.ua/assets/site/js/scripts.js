(function($) {
    $(function() {
        $('.jcarousel').jcarousel().jcarouselAutoscroll({
            interval: 3000,
            target: '+=1',
            autostart: true
        });

        $('.jcarousel-control-prev')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .jcarouselPagination({
                'item': function(page, carouselItems) {
                    return '<a href="#' + page + '"></a>';
                }
            });
    });
})(jQuery);


$(document).ready(function() {
    $("#contents div").hide(); // Скрываем содержание
    $("#tabs li:first").attr("id","current"); // Активируем первую закладку
    $("#contents div:first").fadeIn(); // Выводим содержание
    $("#contents div:first").find('div').fadeIn();
    $('#tabs a').click(function(e) {
        e.preventDefault();
        $("#contents div").hide(); //Скрыть все сожержание
        $("#tabs li").attr("id",""); //Сброс ID
        $(this).parent().attr("id","current"); // Активируем закладку
        $('#' + $(this).attr('title')).fadeIn(); // Выводим содержание текущей закладки
        $('#' + $(this).attr('title')).find('div').fadeIn();
    });
});