KindEditor.ready(function(K) {
    K.create('#desc', {

        //cssPath : ['/packages/kindeditor/plugins/code/prettify.css', 'index.css'],
        autoHeightMode : true, // это автоматическая высота блока
        afterCreate : function() {
            this.loadPlugin('autoheight');
        },
allowFileManager : true,
        width:'100%',
         height:500,
items : [// Вот здесь задаем те кнопки которые хотим видеть
'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
'flash', 'media', 'insertfile', 'table', 'hr', 'emoticons','pagebreak',
'anchor', 'link',  'unlink','map', '|', 'about'
]
});
//Ниже инициализируем доп. например выбор цвета или загрузка файла
var colorpicker;
K('#colorpicker').bind('click', function(e) {
    e.stopPropagation();
    if (colorpicker) {
    colorpicker.remove();
    colorpicker = null;
    return;
    }
var colorpickerPos = K('#colorpicker').pos();
colorpicker = K.colorpicker({
    x : colorpickerPos.x,
    y : colorpickerPos.y + K('#colorpicker').height(),
    z : 19811214,
    selectedColor : 'default',
    noColor : 'Очистить',
    click : function(color) {
    K('#color').val(color);
    colorpicker.remove();
    colorpicker = null;
    }
});
});
K(document).click(function() {
    if (colorpicker) {
    colorpicker.remove();
    colorpicker = null;
    }
});

var editor = K.editor({
    allowFileManager : true
    });
K('#insertfile').click(function() {
    editor.loadPlugin('insertfile', function() {
        editor.plugin.fileDialog({
            fileUrl : K('#url').val(),
            clickFn : function(url, title) {
                K('#url').val(url);
                editor.hideDialog();
            }
});
});
});
K('#image1').click(function() {
    editor.loadPlugin('image', function() {
        editor.plugin.imageDialog({
            imageUrl : K('#url1').val(),
            clickFn : function(url, title, width, height, border, align) {
                K('#url1').val(url);
                editor.hideDialog();
            }
});
});
});
K('#image2').click(function() {
    editor.loadPlugin('image', function() {
        editor.plugin.imageDialog({
            showLocal : false,
            imageUrl : K('#url2').val(),
            clickFn : function(url, title, width, height, border, align) {
                K('#url2').val(url);
                editor.hideDialog();
            }
});
});
});
K('#image3').click(function() {
    editor.loadPlugin('image', function() {
        editor.plugin.imageDialog({
            showRemote : false,
            imageUrl : K('#url3').val(),
            clickFn : function(url, title, width, height, border, align) {
                K('#url3').val(url);
                editor.hideDialog();
            }
});
});
});

});