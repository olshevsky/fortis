<?php
namespace Fortis\Composers;

use Models;

class MenuComposer {

    public function compose($view)
    {
        $view->with('pages', \Page::all());
    }

}