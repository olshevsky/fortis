<?php
namespace Fortis\Composers;

use Models;

class SidebarComposer {

    public function compose($view)
    {
        $categories = \Category::all();
        $parents  = array();
        foreach($categories as $category){
            if(!$category->parent_id){
                $parents[] = $category;
            }
        }
        $view->with('categories', $parents);
    }

}