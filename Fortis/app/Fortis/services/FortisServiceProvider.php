<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 30.08.14
 * Time: 13:04
 */

namespace Fortis\Composers;

use Illuminate\Support\ServiceProvider;

class FortisServiceProvider extends ServiceProvider{
    public function register()
    {
        $this->app->view->composer('includes.site.sidebar', 'Fortis\Composers\SidebarComposer');

        $this->app->view->composer('includes.site.menu', 'Fortis\Composers\MenuComposer');
    }
} 