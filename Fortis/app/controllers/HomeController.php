<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function login()
	{
        return View::make('pages.login')->with('em','');
	}
    public function checkLogin()
    {
        $email = Input::get('email');
        $password = Input::get('password');

        if(Auth::attempt(array('email' => $email, 'password' => $password))){
            return Redirect::to('/admin');
        }
        else{
            return View::make('pages.login')->with('em','Login или пароль введены не верно :(');
        }
    }
    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }

}
