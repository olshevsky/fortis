<?php

class AdminPagesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('pages.admin.pages.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('pages.admin.pages.new')->with('page', new Page)->with('url', '/admin/pages/')->with('method','post');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$page = new Page;
        $page->title = Input::get('title');
        $page->content = Input::get('desc');
        $page->link = Input::get('link');
        $page->meta_desc = Input::get('meta_desc');
        $page->meta_keys = Input::get('meta_keys');
        $page->status = Page::MENU_STATUS;
        $page->role = Page::USERS_ROLE;
        $page->save();
        return Redirect::to('/admin/pages/'.$page->id.'/edit');
    }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('pages.admin.pages.edit')->with('page', Page::find($id))->with('url', '/admin/pages/'.$id.'/')->with('method','put');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $page = Page::find($id);
        $page->title = Input::get('title');
        $page->content = Input::get('desc');
        $page->link = Input::get('link');
        $page->meta_desc = Input::get('meta_desc');
        $page->meta_keys = Input::get('meta_keys');
        $page->save();
        return Redirect::to('/admin/pages/'.$page->id.'/edit');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Page::find($id)->delete();
        return Redirect::to('/admin/pages/');
	}


}
