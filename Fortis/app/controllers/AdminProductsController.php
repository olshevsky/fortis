<?php

class AdminProductsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('pages.admin.products.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $list = array();
        foreach(Category::all() as $category){
            $list[$category->id] = $category->title;
        }
        return View::make('pages.admin.products.new')->with('product', new Product)->with('list', $list)->with('url', '/admin/products/')->with('method','post');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $product = new Product();
        $product->title = Input::get('title');
        $product->desc = Input::get('desc');
        $product->category_id = Input::get('category_id');
        $product->link = Input::get('link');
        $product->meta_keys = Input::get('meta_keys');
        $product->meta_desc = Input::get('meta_desc');
        $product->price = Input::get('price');
        $product->save();

        if(Input::file('fl'))
            foreach(Input::file('fl') as $img)
            {
                $file = new Image();
                if($img)
                {
                    $destinationPath = 'public/uploads';
                    $filename = $img->getClientOriginalName();
                    //$filename .= '.'.$img->getClientOriginalExtension();
                    $upload_success = $img->move($destinationPath, $filename);
                    if($upload_success)
                    {
                        $file->title = $img->getClientOriginalName();
                        $file->desc = $product->title;
                        $file->src = $destinationPath;
                        $file->product_id = $product->id;
                        $file->save();
                    }
                }
            }
        return Redirect::to('/admin/products/'.$product->id.'/edit');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $list = array();
        foreach(Category::all() as $category){
            $list[$category->id] = $category->title;
        }
        return View::make('pages.admin.products.edit')
            ->with('product', Product::find($id))
            ->with('list', $list)
            ->with('url', '/admin/products/'.$id.'/')
            ->with('method','put');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $product = Product::find($id);
        $product->title = Input::get('title');
        $product->desc = Input::get('desc');
        $product->category_id = Input::get('category_id');
        $product->link = Input::get('link');
        $product->meta_keys = Input::get('meta_keys');
        $product->meta_desc = Input::get('meta_desc');
        $product->price = Input::get('price');
        $product->save();

        if(Input::file('fl'))
            foreach(Input::file('fl') as $img)
            {
                $file = new Image();
                if($img)
                {
                    $filename = $img->getClientOriginalName();
                    $upload_success = $img->move(Product::UPLOAD_PATH, $filename);
                    if($upload_success)
                    {
                        $file->title = $img->getClientOriginalName();
                        $file->desc = $product->title;
                        $file->src = Product::DISPLAYED_PATH.$filename;
                        $file->product_id = $product->id;
                        $file->save();
                    }
                }
            }
        return Redirect::to('/admin/products/'.$product->id.'/edit');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$product = Product::find($id);
        foreach($product->images as $image){
            $image->delete();
        }
        $product->delete();
        return Redirect::to('/admin/products/');
	}


}
