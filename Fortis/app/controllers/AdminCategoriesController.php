<?php
class AdminCategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('pages.admin.categories.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $list = $this->setCategories();
        return View::make('pages.admin.categories.new')
            ->with('category', new Category)
            ->with('url', '/admin/categories/')
            ->with('method','post')
            ->with('parent', $list);
	}

    private function setCategories($remove = null)
    {
        $list = array();
        $list[NULL] = '---';
        foreach(Category::all() as $category){
            if(!$category->parent_id)
                $list[$category->id] = $category->title;
        }
        if(!is_null($remove)){
            unset($list[$remove]);
        }
        return $list;
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $list = $this->setCategories();
        $category = new Category();
        $category->title = Input::get('title');
        $category->link = Input::get('link');
        $category->desciption = Input::get('desc');
        $category->meta_keys = Input::get('meta_keys');
        $category->meta_desc = Input::get('meta_desc');

        if(Input::get('parent') != ''){
            $category->parent_id = Input::get('parent');
        }

        $img = Input::file('img');
        if(!is_null($img)){
            $destinationPath = Category::UPLOAD_PATH;
            $filename = $img->getClientOriginalName();
            $upload_success = $img->move($destinationPath, $filename);
            if($upload_success){
                $category->image = Category::DISPLAYED_PATH.$filename;
            }
        }

        $category->save();
        return Redirect::to('/admin/categories/'.$category->id.'/edit');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $category = Category::find($id);
        $list = $this->setCategories($category->id);
        return View::make('pages.admin.categories.edit')
            ->with('category',$category )
            ->with('url', '/admin/categories/'.$id.'/')
            ->with('method','put')
            ->with('parent', $list);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $category = Category::find($id);
        $category->title = Input::get('title');
        $category->link = Input::get('link');
        $category->desciption = Input::get('desc');
        $category->meta_keys = Input::get('meta_keys');
        $category->meta_desc = Input::get('meta_desc');

        if(!is_null(Input::get('parent'))){
            $category->parent()->associate(
                Category::find(Input::get('parent')
                )
            );
        }

        $img = Input::file('img');
        if($img){
            $destinationPath = 'public/uploads';
            $filename = $img->getClientOriginalName();
            $upload_success = $img->move($destinationPath, $filename);
            if($upload_success){
                $category->image = $filename;
            }
        }
        $category->save();
        return Redirect::to('/admin/categories/'.$category->id.'/edit');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Category::find($id)->delete();
        return Redirect::to('/admin/categories/');
	}


}
