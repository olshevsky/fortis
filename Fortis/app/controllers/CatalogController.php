<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 27.08.14
 * Time: 4:45
 */
class CatalogController extends Controller {

    private function setCategories($remove = null)
    {
        $list = array();
        $list[NULL] = '---';
        foreach(Category::all() as $category){
            if(!$category->parent_id)
                $list[$category->id] = $category->title;
        }
        if(!is_null($remove)){
            unset($list[$remove]);
        }
        return $list;
    }

    public function showItem($link)
    {
        $product = Product::where('link',$link)->first();
        if(is_null($product)){
            return App::abort(404);
        }
        return View::make('pages.site.product')
            ->with('product', $product);
    }
    public function showByCategory($link = null)
    {
        if(!is_null($link)){
            $category = Category::where('link',$link)->first();
            if(!$category){
                App::abort(404);
            }
            else{
                if(!is_null($category->parent_id)){
                    return View::make('pages.site.category',
                        array(
                            'products' => $category->products,
                            'category' => $category,
                            'categories' => false,
                            'single' => true,
                        ));
                }
                else{
                    $products = null;
                    foreach($category->children as $child)
                    {
                        if(is_null($products)){
                            $products = $child->products;
                        }
                        else
                            $products = $products->merge($child->products);
                    }
                    return View::make('pages.site.category',
                        array(
                            'products' => $products,
                            'category' => $category,
                            'categories' => false,
                            'single' => true,
                        ));
                }
            }

        } else {
            $categories = array();
            foreach(Category::all() as $category){
                if(!$category->parent_id)
                    $categories[] = $category;
            }

            return View::make('pages.site.category', array(
                    'products' => false,
                    'category' => false,
                    'categories' => $categories,
                    'single' => false,
            ));
        }
    }
} 