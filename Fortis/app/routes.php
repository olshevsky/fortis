<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array(
    'as' => 'home_path',
    function()
    {
        return View::make('pages.site.index');
    }));
Route::get('/page/{page}', array(
    'as' => 'page_path',
    function($page){
        return View::make('pages.site.page')->with('page',Page::where('link',$page)->get()->first());
    }
));

//Route::get('/password', function(){
//    echo Hash::make('fortis_admin');
//});

Route::get('/catalog', array('as'=>'catalog_path', 'uses'=>'CatalogController@showByCategory'));
Route::get('/item/{link}', 'CatalogController@showItem');
Route::get('/category/{link?}', 'CatalogController@showByCategory');
//Route::get('/category', 'CatalogController@showCategory');


Route::get('/login', 'HomeController@login');
Route::post('/login', 'HomeController@checkLogin');
Route::get('/logout ', 'HomeController@logout');

Route::group(array('prefix' => 'admin', 'before' => 'auth'), function () { //'before' => 'auth',
    Route::get('/', 'AdminProductsController@index');

    Route::resource('products', 'AdminProductsController', array('except' => array('show')));
    Route::resource('categories', 'AdminCategoriesController', array('except' => array('show')));
    Route::resource('pages', 'AdminPagesController', array('except' => array('show')));

    Route::delete('image/{image}', function($image){
        $img = Image::find($image);
        $id = $img->product->id;
        $img->delete();
        return Redirect::to('/admin/products/'.$id.'/edit');
    });
});