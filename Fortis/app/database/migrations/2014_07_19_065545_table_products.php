<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableProducts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('products', function($table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('desc');
            $table->string('meta_keys');
            $table->string('meta_desc');
            $table->string('link');
            $table->float('price')->nullable();
            $table->integer('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('products');
	}

}
