<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablePages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pages', function($table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('content');
            $table->string('meta_keys');
            $table->string('meta_desc');
            $table->string('link');
            $table->string('role');
            $table->string('status');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pages');
	}

}
