<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('categories', function($table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('desciption');
            $table->string('meta_keys');
            $table->string('meta_desc');
            $table->string('link');
            $table->integer('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('categories');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('categories');
	}

}
