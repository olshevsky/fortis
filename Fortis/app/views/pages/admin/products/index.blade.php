@extends('layouts.admin')
@section('content')
    <h1>Все товары</h1>
    <p><a href="/admin/products/create" class="btn btn-success">Добавить товар</a></p>
    <table class="table table-bordered">
        <thead>
            <td>Изображение</td>
            <td>Название</td>
            <td>Цена</td>
            <td>Операции</td>
        </thead>
        <tbody>
        @foreach (Product::all() as $product)
            <tr>
                <td>
                    @if($product->images->count() != 0)
                        <img src="{{ asset($product->images->first()['src']) }}" width='100' />
                    @endif
                </td>
                <td>
                    <a href="/admin/products/{{ $product->id }}/edit">{{ $product->title }}</a>
                </td>
                <td>
                    {{ $product->price }}
                </td>
                <td>
                    <a href="/admin/products/{{ $product->id }}/edit" class="btn btn-primary btn-small">Изменить</a>
                    {{ Form::open(array('url' => '/admin/products/'.$product->id, 'method' => 'delete')) }}
                        <button type="submit" class="btn btn-danger btn-small">Удалить</button>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop