@extends('layouts.admin')
@section('content')

    <h1>Редактирование товара</h1>

    @include('includes.admin.forms.product_form')

    <table class="table table-bordered">
        @foreach ($product->images as $image)
            <tr>
                <td>
                    <img src="{{ asset($image->src); }}" height='100' />
                </td>
                <td>
                    {{ $image->src }}
                </td>
                <td>
                    {{ Form::open(array('url' => '/admin/image/'.$image->id, 'method' => 'delete')) }}
                        <button type="submit" class="btn btn-danger">Delete</button>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
    </table>

    {{ HTML::script('/packages/ked/kindeditor/kindeditor.js') }}
    {{ HTML::script('/packages/ked/kindeditor/ru_RU.js') }}
    {{ HTML::script('/packages/ked/kindeditor/install.js') }}
    {{ HTML::script('/assets/admin/js/translit.js') }}
    {{ HTML::script('/assets/admin/js/images_upload.js') }}
    {{ HTML::script('/assets/admin/js/new_product.js') }}
@stop