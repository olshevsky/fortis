@extends('layouts.admin')
@section('content')

    <h1>Редактирование страницы</h1>

    @include('includes.admin.forms.page_form')

    {{ HTML::script('/packages/ked/kindeditor/kindeditor.js') }}
    {{ HTML::script('/packages/ked/kindeditor/ru_RU.js') }}
    {{ HTML::script('/packages/ked/kindeditor/install.js') }}
    {{ HTML::script('/assets/admin/js/translit.js') }}
    {{ HTML::script('/assets/admin/js/images_upload.js') }}
    {{ HTML::script('/assets/admin/js/new_product.js') }}
@stop