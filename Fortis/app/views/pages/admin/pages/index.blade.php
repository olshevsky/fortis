@extends('layouts.admin')
@section('content')
    <h1>Все страницы</h1>
    <p><a href="/admin/pages/create" class="btn btn-success">Добавить страницу</a></p>
    <table class="table table-bordered">
        <thead>
            <td>Название</td>
            <td>Описание</td>
            <td>Ссылка</td>
            <td>Операции</td>
        </thead>
        <tbody>
        @foreach (Page::all() as $page)
            <tr>
                <td>
                    <a href="/admin/pages/{{ $page->id }}/edit">{{ $page->title }}</a>
                </td>
                <td>
                    {{ $page->meta_desc }}
                </td>
                <td>
                    {{ $page->link }}
                </td>
                <td>
                    @if($page->status != Page::SYSTEM_ROLE)
                        <a href="/admin/pages/{{ $page->id }}/edit" class="btn btn-primary btn-small">Изменить</a>
                        {{ Form::open(array('url' => '/admin/pages/'.$page->id, 'method' => 'delete')) }}
                            <button type="submit" class="btn btn-danger btn-small">Удалить</button>
                        {{ Form::close() }}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop