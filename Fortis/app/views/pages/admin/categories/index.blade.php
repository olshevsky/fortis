@extends('layouts.admin')
@section('content')
    <h1>Все категории</h1>
    <p><a href="/admin/categories/create" class="btn btn-success">Добавить категорию</a></p>
    <table class="table table-bordered">
        <thead>
            <td>Изображение</td>
            <td>Название</td>
            <td>Операции</td>
        </thead>
        <tbody>
        @foreach (Category::all() as $category)
            <tr>
                <td>
                    <img src="{{ asset($category->image) }}" width='100' />
                </td>
                <td>
                    <a href="/admin/categories/{{ $category->id }}/edit">{{ $category->title }}</a>
                </td>
                <td>
                    <a href="/admin/categories/{{ $category->id }}/edit" class="btn btn-primary btn-small">Изменить</a>
                    {{ Form::open(array('url' => '/admin/categories/'.$category->id, 'method' => 'delete')) }}
                        <button type="submit" class="btn btn-danger btn-small">Удалить</button>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop