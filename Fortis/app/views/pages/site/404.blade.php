@extends('layouts.home')
@section('content')
    <section id="content" style="text-align:center;">
        <img src="/assets/site/img/Logotip.gif" alt="Fortis"/>
        <h2>Упс... Что-то пошло не так :(</h2>
        <h3>Запрашиваемая страница не существует или в разработке</h3>
    </section>
@stop