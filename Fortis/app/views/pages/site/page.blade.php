@extends('layouts.home')
@section('meta_keys')
    @if(!empty($category))
        <meta name="description" content="{{ $page->meta_desc }}">
        <meta name="keywords" content="{{ $page->meta_keys }}">
        <meta name="author" content="Fortis inc.">
    @else
        <meta name="description" content="Fortis | Каталог продукции">
        <meta name="keywords" content="Fortis, холодная сварка, холодная сварка Алмаз, холодная сварка Fortis">
        <meta name="author" content="Fortis inc.">
    @endif
@stop
@section('content')
    <link type="text/css" rel="stylesheet" href="/assets/site/css/styles_page.css">
    <section id="content">
        @include('includes.site.sidebar')
        <section class="page">
            <h1 class="header">{{ $page->title }}</h1>
            {{ $page->content }}
        </section>
    </section>
@stop