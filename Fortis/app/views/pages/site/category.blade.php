@extends('layouts.home')
@section('meta_keys')
    @if(!empty($category))
        <meta name="description" content="{{ $category->meta_desc }}">
        <meta name="keywords" content="{{ $category->meta_keys }}">
        <meta name="author" content="Fortis inc.">
    @else
        <meta name="description" content="Fortis | Каталог продукции">
        <meta name="keywords" content="Fortis, холодная сварка, холодная сварка Алмаз, холодная сварка Fortis">
        <meta name="author" content="Fortis inc.">
    @endif
@stop
@section('content')
    <link type="text/css" rel="stylesheet" href="/assets/site/css/styles_categories.css">
    <section id="content" style="text-align:center;">

        @include('includes.site.sidebar')
        <section id="categoryWrap">
            @if (!$single)
                <ul class="cat_table">
                    <?php $el = 1; ?>
                    @foreach($categories as $category)
                        @if ($el == 1)
                            <div class="row">
                        @endif

                        <li class="item" style="background: url('{{ asset($category->image) }}') no-repeat; background-position: center top;">
                            <h3><a href="/category/{{ $category->link }}">{{ $category->title }}</a></h3>
                            <p>{{ $category->desciption }}</p>
                        </li>
                        @if($el == 3)
                            </div>
                             <?php $el = 1; ?>
                        @else
                            <?php $el++; ?>
                        @endif
                    @endforeach
                </ul>
            @else
                <h1>Продукция</h1>
                <h2 class="header" style="color: #4e4e4e">{{ $category->title }}</h2>
                    @if(count($products) <= 0)
                        <h3>В данной категории еще нет товаров, но мы над этим работаем!</h3>
                    @else
                        <ul class="cat_table">
                            <?php $el = 1; ?>
                            @foreach($products as $product)
                                @if ($el == 1)
                                <div class="row">
                                    @endif
                                    @if(isset($product->images->first()->src))
                                        <li class="item" style="background: url('{{ asset($product->images->first()->src) }}') no-repeat; background-size: 125%; background-position: center top;">
                                            <h3><a href="/item/{{ $product->link }}">{{ $category->title }}</a></h3>
                                            <p><?php echo (!is_null($product->meta_desc)? $product->meta_desc : '')?></p>
                                        </li>
                                    @else
                                        <li class="item" style="background: url('') no-repeat; background-size: 125%; background-position: center top;">
                                            <h3><a href="/item/{{ $product->link }}">{{ $category->title }}</a></h3>
                                            <p><?php echo (!is_null($product->meta_desc)? $product->meta_desc : '')?></p>
                                        </li>
                                    @endif
                                    @if($el == 3)
                                </div>
                                    <?php $el = 1; ?>
                                @else
                                    <?php $el++; ?>
                                @endif
                            @endforeach
                        </ul>
                    @endif
            @endif
        </section>

    </section>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>
        $(function () {                                      // Когда страница загрузится
            $('a').each(function () {             // получаем все нужные нам ссылки
                var location = window.location.href; // получаем адрес страницы
                var link = this.href;                // получаем адрес ссылки
                if(location == link) {               // при совпадении адреса ссылки и адреса окна
                    $(this).addClass('active');  //добавляем класс
                }
            });
        });
    </script>
@stop