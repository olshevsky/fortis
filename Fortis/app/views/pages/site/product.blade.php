@extends('layouts.home')
@section('meta_keys')
    @if(!empty($category))
        <meta name="description" content="{{ $product->meta_desc }}">
        <meta name="keywords" content="{{ $product->meta_keys }}">
        <meta name="author" content="Fortis inc.">
    @else
        <meta name="description" content="Fortis | Каталог продукции">
        <meta name="keywords" content="Fortis, холодная сварка, холодная сварка Алмаз, холодная сварка Fortis">
        <meta name="author" content="Fortis inc.">
    @endif
@stop
@section('content')
    <link type="text/css" rel="stylesheet" href="/assets/site/css/styles_product.css">
    <section id="content" style="text-align:center;">

        @include('includes.site.sidebar')

        <h1>{{ $product->title }}</h1>
        <section id="productView">
            <div class="imageSide">
                <div class="zoomer">
                    <img src="" alt="" class="bigImg"/>
                </div>

                <div class="thumbs">
                    <ul class="in-row">
                        @foreach ($product->images as $image)
                            <li><img src="{{ asset($image->src); }}" alt="" class="thumb"></li>
                        @endforeach
                    </ul>
                </div>
                <ul class="icn-desc">
                    <li class="vlazn">Клеит влажные поверхности</li>
                    <li class="shov">Жесткий шов</li>
                    <li class="bistro">Быстрый результат</li>
                </ul>
            </div>
            <div class="descSide">
                {{ $product->desc }}
            </div>
        </section>
    </section>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.bigImg').attr('src', $('.thumb').first().attr('src'));
            $('.thumb').first().addClass('activeImg');
        });
        $('.thumb').on('click',function(){
            $('.thumb').removeClass('activeImg');
            $('.bigImg').attr('src', $(this).attr('src'));
            $(this).addClass('activeImg');
        })
    </script>
@stop