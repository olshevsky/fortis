@extends('layouts.home')
@section('content')
    <section id="content">
        <div class="jcarousel-wrapper">
            <div class="jcarousel">
                <ul>
                    <!-- <li><img src="../_shared/img/img1.jpg" width="600" height="400" alt=""></li> -->
                    <li><img src="/assets/site/img/first_slide.png" alt="" /></li>
                    <li><img src="/assets/site/img/second_slide.png" alt="" /></li>
                    <li><img src="/assets/site/img/rd_slide.png" alt=""/></li>
                </ul>
            </div>

            <p class="jcarousel-pagination"></p>
        </div>
        <div class="bottom-block">
            <div class="text">
                <p>
                    Fortis является производителем высококачественной продукции бытового и производственного назначения. Компания является официальным представителем ТМ Алмаз в Украине, а также предлагает рынку линейку своих оригинальных товаров.
                </p>
                <p>
                    Мы производим полимерный клей-холодную сварку на эпоксидной основе с добавлением минеральных и железных наполнителей. Клей может соединять разнообразные по структуре и свойствам материалы. Наша продукция максимально приспособлена для работы и проста в применении.
                </p>
                <p>
                    Мы ценим своих клиентов и поэтому стремимся не только быстро и качественно выполнить свои обязательства по изготовлению и доставке нашей продукции, но и предложить заказчику индивидуальную модель сотрудничества.</p>
                </p>
            </div>
            <h2>наша продукция это:</h2>
            <ul class="products">
                <div class="in-row">
                    <li class="star">
                        <h2 class="tl">ЧЕСТНАЯ ЦЕНА</h2>
                        <p class="desc">Не затрачивайте лишние средства и экономьте с нами</p>
                    </li>
                    <li class="time">
                        <h2 class="tl">БЫСТРЫЙ РЕЗУЛЬТАТ</h2>
                        <p class="desc">Достигайте поставленные цели в короткие сроки</p>
                    </li>
                    <li class="strong">
                        <h2 class="tl">НАДЕЖНОСТЬ</h2>
                        <p class="desc">Делайте один раз: качественно и надолго</p>
                    </li>
                    <li class="price">
                        <h2 class="tl">ШИРОКИЙ АССОРТИМЕНТ</h2>
                        <p class="desc">Охватывайте большой круг различных задач и проблем</p>
                    </li>
                </div>
            </ul>
        </div>

        <div class="prod-block">
            <ul id="tabs">
                <li><a href="#" title='prod'>Продукция</a></li>
                <li><a href="#" title='ysl'>Услуги</a></li>
            </ul>
            <div id="contents">
                <div id="prod">
                    <ul>
                        <div class="in-row">
                            <li class="fortis">
                                <div class="blck">
                                    <h3><a href="#">Клей-холодная сварка "Fortis"</a></h3>
                                    <p class="desc">Клей на эпоксидной основе с минеральными и железными наполнителями</p>
                                </div>
                            </li>
                            <li class="almaz">
                                <div class="blck">
                                    <h3><a href="#">Клей-холодная сварка "Алмаз"</a></h3>
                                    <p class="desc">Клей на эпоксидной основе с минеральными и железными наполнителями</p>
                                </div>
                            </li>
                            <li class="edp">
                                <div class="blck">
                                    <h3><a href="#">Эпоксидный клей ЭДП</a></h3>
                                    <p class="desc">Эпоксидный универсальный клей, являющий собой модифицированную эпоксидную смолу с отвердителем</p>
                                </div>
                            </li>
                            <li class="lenta">
                                <div class="blck">
                                    <h3><a href="#">Скотчи, ленты</a></h3>
                                    <p class="desc">Продукция для применения в быту и на производстве</p>
                                </div>
                            </li>
                        </div>
                        <div class="in-row">
                            <li class="bars">
                                <div class="blck">
                                    <h3><a href="#">Герметики, мастики, гидроизоляция</a></h3>
                                    <p class="desc">Строительные материалы для защиты объектов от воды.</p>
                                </div>
                            </li>
                            <li class="grunt">
                                <div class="blck">
                                    <h3><a href="#">Грунтовки, пластификаторы</a></h3>
                                    <p class="desc">Разнообразная продукция для улучшения качества строительных работ</p>
                                </div>
                            </li>
                            <li class="perch">
                                <div class="blck">
                                    <h3><a href="#">Перчатки</a></h3>
                                    <p class="desc">Надежная защита и мера предосторожности организма</p>
                                </div>
                            </li>
                            <li class="lamp">
                                <div class="blck">
                                    <h3><a href="#">Светодиодная продукция</a></h3>
                                    <p class="desc">Продукция для бытового, уличного и промышленного освещен</p>
                                </div>
                            </li>
                        </div>
                    </ul>
                </div>
                <div id="ysl">
                    <ul>
                        <div class="in-row">
                            <li class="home">
                                <div class="title">
                                    <p>Строительные и ремонтные работы в Харькове</p>
                                </div>
                            </li>
                            <li class="svarka">
                                <div class="title">
                                    <p>Металлоконструкции</p>
                                </div>
                            </li>
                            <li class="teplo">
                                <div class="title">
                                    <p>Утепление домов</p>
                                </div>
                            </li>
                        </div>
                        <div class="in-row">
                            <li class="roof">
                                <div class="title">
                                    <p>Мягкая кровля, укладка и ремонт</p>
                                </div>
                            </li>
                            <li class="santeh">
                                <div class="title">
                                    <p>Сантехнические работы</p>
                                </div>
                            </li>
                            <li class="condition">
                                <div class="title">
                                    <p>Кондиционирование</p>
                                </div>
                            </li>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
        <br />
    </section>
@stop