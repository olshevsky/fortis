<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Fortis</title>
        <link rel="shortcut icon" href="fav.ico" />
        <link rel="stylesheet/css" type="text/css" href="/assets/site/css/jcarousel.css" />
        <link rel="stylesheet" type="text/css" href="/assets/site/css/styles.css" />
        @yield('meta_keys')
       </head>
    <body>
        <div id="wrap">
            @include('includes.site.header')
            @include('includes.site.menu')
            @yield('content')
            <div class="clear"></div>
        </div>
        @include('includes.site.footer')
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="/assets/site/js/jquery.jcarousel.min.js"></script>
        <script src="/assets/site/js/scripts.js"></script>
    </body>
</html>