<html>
<head>
	<meta charset="UTF-8">
	<title>Fortis | Панель администратора</title>
    {{ HTML::style('/packages/bootstrap/css/bootstrap.css') }}
    <link rel="stylesheet/less" type="text/css" href="/assets/admin/css/style.less" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    {{ HTML::script('/packages/less/less.min.js') }}
</head>
<body>
	<div class="container">
		@include('includes.admin.menu')
		<div class="content">
			@yield('content')
		</div>
		<div class="clear"></div>
	</div>
	<footer>
			<span class="copy">&copy FORTIS</span>
			<span class="dev">Разработка сайта - <a href="#">Olshevsky Igor</a> | Дизайн - <a href="#">Маслов Сергей</a></span>
	</footer>
</body>
</html>