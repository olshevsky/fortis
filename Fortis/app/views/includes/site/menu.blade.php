<ul id="menu">
    <div class="nav-row">
        <li @if(route('home_path',array()) ==Request::url()) class="active" @endif>
            <a href="/">Главная</a>
        </li>
        <li @if(route('catalog_path',array()) ==Request::url()) class="active" @endif>
            <a href="/catalog">Продукция</a>
        </li>
        @foreach($pages as $page)
            <li @if(route('page_path', ['page' => $page->link]) ==Request::url()) class="active" @endif>
                <a href="/page/{{ $page->link }}">{{ $page->title }}</a>
            </li>
        @endforeach
    </div>
</ul>