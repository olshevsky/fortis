<section id="sidebar">
    <ul class="menu">
        @foreach ($categories as $category)
        <li>
            <ul class="category">
                @if (count($category->children) != 0)
                    <li class="category-head"><a href="/category/{{ $category->link }}">{{ $category->title }}</a></li>
                @else
                    <li class="category-head single"><a href="/category/{{ $category->link }}">{{ $category->title }}</a></li>
                @endif
                @foreach ($category->children as $child)
                    <li><a href="/category/{{ $child->link }}">{{ $child->title }}</a></li>
                @endforeach
            </ul>
        </li>
        @endforeach
    </ul>
</section>