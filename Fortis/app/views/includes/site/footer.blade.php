<footer>
    <h3>"FORTIS"</h3>
    <h2 class="devs">Дизайн - <a href='http://vk.com/prorok90'>Маслов Сергей</a> | Разработка сайта - <a href='http://vk.com/igor_olshevsky'>Ольшевский Игорь</a></h2>
    <ul class="bars">
        <li class="social">
            <ul class="icons">
                <li><a class="vk" href="#">VK</a></li>
                <li><a class="fb" href="#">Facebook</a></li>
                <li><a class="yt" href="#">YouTube</a></li>
            </ul>
        </li>
        <li class="cont">
            <ul class="ph">
                <li>svarka-almaz@ukr.net</li>
                <li>+38 057 764 75 15</li>
                <li>+38 093 147 68 20</li>
                <li>+38 050 343 24 18</li>
            </ul>
        </li>
        <li class="brands">
            <ul class="brand-list">
                <li class="fortis"></li>
                <li class="almaz"></li>
                <li class="bethel"></li>
            </ul>
        </li>
    </ul>
</footer>