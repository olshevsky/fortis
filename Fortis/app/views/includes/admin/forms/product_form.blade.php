{{ Form::open(array('url' => $url, 'files' => true, 'method' => $method)) }}
{{ Form::label('title', 'Название'); }}
{{ Form::text('title', $product->title) }}
<br />
{{ Form::label('link', 'Ссылка'); }}
{{ Form::text('link', $product->link) }}
<br />
{{ Form::label('price', 'Цена'); }}
{{ Form::text('price', $product->price) }}
<br />
{{ Form::label('category_id', 'Категория'); }}
{{ Form::select('category_id',$list, $product->category_id) }}

{{ Form::label('desc', 'Описание'); }}
{{ Form::textarea('desc',$product->desc,array('id' => 'desc')) }}

<div class="seo">
        <span>
            {{ Form::label('desc', 'SEO: ключевые слова'); }}
            {{ Form::textarea('meta_keys',$product->meta_keys,array('id' => 'meta_keys')) }}
        </span>
        <span>
            {{ Form::label('desc', 'SEO: описание страницы'); }}
            {{ Form::textarea('meta_desc',$product->meta_desc,array('id' => 'meta_desc')) }}
        </span>
</div>

<br />
<span href="#" class="add btn btn-success">Добавить изображение</span>
<br />
<div class="inputs">
    <div class="pole">
        <img src="#" class="imagePreview" style="display: none;"/>
        {{ Form::file('fl[]'); }}
        <a class="btn btn-danger delete">X</a>
    </div>
</div>
<br />
<p>{{ Form::submit('Сохранить', array('class' => 'btn btn-primary')); }}</p>
{{ Form::close() }}