{{ Form::open(array('url' => $url, 'files' => true, 'method' => $method)) }}

{{ Form::label('title', 'Название'); }}
{{ Form::text('title', $category->title) }}
<br />
{{ Form::label('link', 'Ссылка'); }}
{{ Form::text('link', $category->link) }}

<br />
@if ($category->parent != '')
    <p>
        <b>Родительская категория: </b>{{ $category->parent->title }}
    </p>
    {{ Form::label('parent', 'Категория'); }}
    {{ Form::select('parent', $parent, $category->parent->id) }}
@else
    {{ Form::label('parent', 'Категория'); }}
    {{ Form::select('parent', $parent, $category->parent) }}
@endif

{{ Form::label('desc', 'Описание'); }}
{{ Form::textarea('desc',$category->desciption,array('id' => 'desc')) }}

<div class="seo">
        <span>
            {{ Form::label('desc', 'SEO: ключевые слова'); }}
            {{ Form::textarea('meta_keys',$category->meta_keys,array('id' => 'meta_keys')) }}
        </span>
        <span>
            {{ Form::label('desc', 'SEO: описание страницы'); }}
            {{ Form::textarea('meta_desc',$category->meta_desc,array('id' => 'meta_desc')) }}
        </span>
</div>

<br />
<div class="inputs">
    <div class="pole">
        <img src="#" class="imagePreview" style="display: none;"/>
        {{ Form::file('img'); }}
    </div>
</div>
<br />
<p>{{ Form::submit('Сохранить', array('class' => 'btn btn-primary')); }}</p>
{{ Form::close() }}