{{ Form::open(array('url' => $url, 'method' => $method)) }}

{{ Form::label('title', 'Название'); }}
{{ Form::text('title', $page->title) }}
<br />
{{ Form::label('link', 'Ссылка'); }}
{{ Form::text('link', $page->link) }}

{{ Form::label('desc', 'Описание'); }}
{{ Form::textarea('desc',$page->content,array('id' => 'desc')) }}

<div class="seo">
        <span>
            {{ Form::label('desc', 'SEO: ключевые слова'); }}
            {{ Form::textarea('meta_keys',$page->meta_keys,array('id' => 'meta_keys')) }}
        </span>
        <span>
            {{ Form::label('desc', 'SEO: описание страницы'); }}
            {{ Form::textarea('meta_desc',$page->meta_desc,array('id' => 'meta_desc')) }}
        </span>
</div>

<br />
<p>{{ Form::submit('Сохранить', array('class' => 'btn btn-primary')); }}</p>
{{ Form::close() }}