<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">Fortis</a>
        <ul class="nav">
            <li @if(Route::getCurrentRoute()->getPath()=='admin') class="active" @endif>
                <a href="/admin">Главная</a>
            </li>
            <li @if(Route::getCurrentRoute()->getPath()=='admin/products') class="active" @endif>
                <a href="/admin/products/">Товары</a>
            </li>
            <li @if(Route::getCurrentRoute()->getPath()=='admin/categories') class="active" @endif>
                <a href="/admin/categories/">Категории</a>
            </li>
            <li @if(Route::getCurrentRoute()->getPath()=='admin/pages') class="active" @endif>
                <a href="/admin/pages/">Страницы</a>
            </li>
            <li @if(Route::getCurrentRoute()->getPath()=='admin/messages') class="active" @endif>
                <a href="#">Сообщения</a>
            </li>
        </ul>
    </div>
</div>