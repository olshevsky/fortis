<?php
class Image extends Eloquent {

    protected $table = 'images';
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('Product', 'product_id');
    }
}