<?php
class Product extends Eloquent{

    const UPLOAD_PATH = "public/assets/site/img/";
    const DISPLAYED_PATH = "/assets/site/img/";

    protected $table = 'products';
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('Category', 'category_id');
    }

    public function images()
    {
        return $this->hasMany('Image');
    }

}