<?php
class Category extends Eloquent{

    //Paths
    const UPLOAD_PATH = "public/assets/site/img/";
    const DISPLAYED_PATH = "/assets/site/img/";

    protected $table = 'categories';
    public $timestamps = false;

    public function products()
    {
        return $this->hasMany('Product');
    }

    public function children()
    {
        return $this->hasMany('Category', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('Category', 'id');
    }
}