<?php
class Page extends Eloquent{

    // Roles
    const SYSTEM_ROLE = 'system';
    const USERS_ROLE = 'role';

    //Statuses
    const MENU_STATUS = 'menu';
    const STATUS_HIDDEN = 'hidden';

    protected $table = 'pages';
    public $timestamps = false;
}